package com.example.truthaboutdatabase.models;

public class Track {
    private Integer trackId;
    private String name;
    private Integer songLengthInMilliseconds;
    private Double price;

    public Track(Integer trackId, String name, Integer songLengthInMilliseconds, Double price) {
        this.trackId = trackId;
        this.name = name;
        this.songLengthInMilliseconds = songLengthInMilliseconds;
        this.price = price;
    }

    public Integer getTrackId() {
        return trackId;
    }

    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSongLengthInMilliseconds() {
        return songLengthInMilliseconds;
    }

    public void setSongLengthInMilliseconds(Integer songLengthInMilliseconds) {
        this.songLengthInMilliseconds = songLengthInMilliseconds;
    }

    public String getSongInMinutes() {
        int minutes = ((songLengthInMilliseconds/1000) % 3600) / 60;
        int seconds = ((songLengthInMilliseconds/1000) % 60);

        return String.format("%02d:%02d",minutes,seconds);
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
