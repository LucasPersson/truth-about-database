package com.example.truthaboutdatabase.models;

public class CustomerSpender {
    private Integer customerId;
    private Double totalSpentOnInvoice;

    public CustomerSpender(Integer customerId, Double totalSpentOnInvoice) {
        this.customerId = customerId;
        this.totalSpentOnInvoice = totalSpentOnInvoice;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Double getTotalSpentOnInvoice() {
        return totalSpentOnInvoice;
    }

    public void setTotalSpentOnInvoice(Double totalSpentOnInvoice) {
        this.totalSpentOnInvoice = totalSpentOnInvoice;
    }
}
