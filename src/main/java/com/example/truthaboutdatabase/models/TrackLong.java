package com.example.truthaboutdatabase.models;

import java.util.List;

public class TrackLong extends Track{

    private String album;
    private String genre;
    private String artists;

    public TrackLong(Integer trackId, String name, Integer songLengthInMilliseconds, Double price, String album, String genre, String artists) {
        super(trackId, name, songLengthInMilliseconds, price);
        this.album = album;
        this.genre = genre;
        this.artists = artists;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }
}
