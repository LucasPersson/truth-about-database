package com.example.truthaboutdatabase.models;

import java.util.List;

public class CustomerGenre {

    private List<String> mostPopularGenre;

    public CustomerGenre(List<String> mostPopularGenre) {
        this.mostPopularGenre = mostPopularGenre;
    }

    public List<String> getMostPopularGenre() {
        return mostPopularGenre;
    }

    public void setMostPopularGenre(List<String> mostPopularGenre) {
        this.mostPopularGenre = mostPopularGenre;
    }
}
