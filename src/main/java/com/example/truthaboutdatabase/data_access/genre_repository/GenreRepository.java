package com.example.truthaboutdatabase.data_access.genre_repository;

import com.example.truthaboutdatabase.models.Genre;

import java.util.List;

public interface GenreRepository {
    List<Genre> getRandomGenres();
}
