package com.example.truthaboutdatabase.data_access.genre_repository;

import com.example.truthaboutdatabase.data_access.utils.DatabaseConnectionFactory;
import com.example.truthaboutdatabase.models.Genre;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GenreRepositoryImplementation implements GenreRepository{
    private final DatabaseConnectionFactory connectionFactory;
    // gets database dependency through dependency injection
    public GenreRepositoryImplementation(DatabaseConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Genre> getRandomGenres() {
        List<Genre> genres = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Genre ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                genres.add(new Genre(
                            resultSet.getInt("GenreId"),
                            resultSet.getString("Name")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return genres;
    }
}
