package com.example.truthaboutdatabase.data_access.customer_repository;

import com.example.truthaboutdatabase.data_access.utils.DatabaseConnectionFactory;
import com.example.truthaboutdatabase.models.Customer;
import com.example.truthaboutdatabase.models.CustomerCountry;
import com.example.truthaboutdatabase.models.CustomerGenre;
import com.example.truthaboutdatabase.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImplementation implements CustomerRepository {
    private final DatabaseConnectionFactory connectionFactory;
    // gets database dependency through dependency injection
    public CustomerRepositoryImplementation(DatabaseConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        Customer customer = null;
        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = ?");
            preparedStatement.setInt(1,customerId);
            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return customer;
    }

    @Override
    public List<Customer> getCustomerByName(String firstName, String lastName) {
        List<Customer> customers = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE ? AND LastName LIKE ?");
            preparedStatement.setString(1,"%" + firstName + "%");
            preparedStatement.setString(2,"%" + lastName + "%");

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    @Override
    public List<Customer> getLimitedCustomers(Integer limit, Integer offset) {
        List<Customer> customers = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1,limit);
            preparedStatement.setInt(2,offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customers;
    }

    @Override
    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try(Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1,customer.getFirstName());
            preparedStatement.setString(2,customer.getLastName());
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setString(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhoneNumber());
            preparedStatement.setString(6,customer.getEmail());

            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return success;
    }

    @Override
    public Boolean updateCustomer(String id, Customer customer) {
        Boolean success = false;
        try(Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement("UPDATE customer SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setString(1,customer.getFirstName());
            preparedStatement.setString(2,customer.getLastName());
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setString(4,customer.getPostalCode());
            preparedStatement.setString(5,customer.getPhoneNumber());
            preparedStatement.setString(6,customer.getEmail());
            preparedStatement.setString(7,id);

            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return success;
    }

    @Override
    public List<CustomerCountry> getCustomersInCountries() {

        List<CustomerCountry> customerCountries = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement("select Country, count(CustomerId) as Customers from Customer group by Country order by Customers desc;");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerCountries.add(new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("Customers")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerCountries;
    }

    @Override
    public List<CustomerSpender> getHighestSpenders() {

        List<CustomerSpender> customerSpenders = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Customer.CustomerId, SUM(Total) as TotalInvoice from Customer" +
            " join Invoice I on Customer.CustomerId = I.CustomerId group by Customer.CustomerId order by TotalInvoice desc;");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerSpenders.add(new CustomerSpender(
                        resultSet.getInt("CustomerId"),
                        resultSet.getDouble("TotalInvoice")
                ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerSpenders;
    }

    @Override
    public CustomerGenre getCustomerMostPopularGenre(Integer id) {

        List<String> genres = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {

            // Specific query with that count all songs for each genre for a specific customer
            PreparedStatement preparedStatement = conn.prepareStatement("""
                    select count(G.GenreId) Tracks, G.Name from Track
                    join Genre G on Track.GenreId = G.GenreId
                    where TrackId in (select TrackId from InvoiceLine where InvoiceId in
                            (select InvoiceId from Invoice where Invoice.CustomerId in
                            (select CustomerId from Customer where CustomerId = ?)))
                    group by G.Name having Tracks = (select max(TrackAmount) from
                            (select count(TrackId) as TrackAmount from Track
                            join Genre G on Track.GenreId = G.GenreId
                            where TrackId in
                            (select TrackId from InvoiceLine where InvoiceId in
                            (select InvoiceId from Invoice where Invoice.CustomerId in
                            (select CustomerId from Customer where CustomerId = ?)))
                    group by G.Name));""");
            
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(
                        resultSet.getString("Name")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new CustomerGenre(genres);
    }
}

