package com.example.truthaboutdatabase.data_access.customer_repository;

import com.example.truthaboutdatabase.models.Customer;
import com.example.truthaboutdatabase.models.CustomerCountry;
import com.example.truthaboutdatabase.models.CustomerGenre;
import com.example.truthaboutdatabase.models.CustomerSpender;

import java.util.List;
import java.util.Map;

public interface CustomerRepository {
    List<Customer> getAllCustomers();
    Customer getCustomerById(Integer customerId);
    List<Customer> getCustomerByName(String firstName, String lastName);
    List<Customer> getLimitedCustomers(Integer limit, Integer offset);
    Boolean addCustomer(Customer customer);
    Boolean updateCustomer(String id,Customer customer);
    List<CustomerCountry> getCustomersInCountries();
    List<CustomerSpender> getHighestSpenders();
    CustomerGenre getCustomerMostPopularGenre(Integer id);

}
