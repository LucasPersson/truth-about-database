package com.example.truthaboutdatabase.data_access.track_repository;

import com.example.truthaboutdatabase.data_access.utils.DatabaseConnectionFactory;
import com.example.truthaboutdatabase.models.Track;
import com.example.truthaboutdatabase.models.TrackLong;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TrackRepositoryImplementation implements TrackRepository{
    private final DatabaseConnectionFactory connectionFactory;
    // gets database dependency through dependency injection
    public TrackRepositoryImplementation(DatabaseConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Track> getRandomTracks() {
        List<Track> tracks = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {
            // Connect to DB
            // Make SQL query
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT TrackId,Name,Milliseconds,UnitPrice FROM Track ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                tracks.add(new Track(
                            resultSet.getInt("TrackId"),
                            resultSet.getString("Name"),
                            resultSet.getInt("Milliseconds"),
                            resultSet.getDouble("UnitPrice")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tracks;
    }

    @Override
    public List<TrackLong> getSearchResults(String searchString) {
        List<TrackLong> tracks = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {

            // join all the data for artis track genre and album to search for with searchbar
            PreparedStatement preparedStatement = conn.prepareStatement("""
                select TrackId, Track.name, Milliseconds, UnitPrice, a.Title as Album, g.Name as Genre, Artist.Name as Artist from Track
                    join Album A on Track.AlbumId = A.AlbumId
                    join Genre G on G.GenreId = Track.GenreId
                    join Artist on Artist.ArtistId = A.ArtistId
                    where Track.Name like ?
                """);
            preparedStatement.setString(1, "%" + searchString + "%");

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                tracks.add(new TrackLong(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("Name"),
                        resultSet.getInt("Milliseconds"),
                        resultSet.getDouble("UnitPrice"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre"),
                        resultSet.getString("Artist")
                    )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tracks;
    }
}
