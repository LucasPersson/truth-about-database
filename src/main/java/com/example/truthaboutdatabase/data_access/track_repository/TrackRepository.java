package com.example.truthaboutdatabase.data_access.track_repository;

import com.example.truthaboutdatabase.models.Track;
import com.example.truthaboutdatabase.models.TrackLong;

import java.util.List;

public interface TrackRepository {
    List<Track> getRandomTracks();
    List<TrackLong> getSearchResults(String searchString);
}
