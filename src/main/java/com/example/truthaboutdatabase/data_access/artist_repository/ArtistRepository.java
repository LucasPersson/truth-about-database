package com.example.truthaboutdatabase.data_access.artist_repository;

import com.example.truthaboutdatabase.models.Artist;

import java.util.List;

public interface ArtistRepository {

    List<Artist> getRandomArtists();
}
