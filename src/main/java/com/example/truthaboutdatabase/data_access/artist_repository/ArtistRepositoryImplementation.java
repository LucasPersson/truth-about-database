package com.example.truthaboutdatabase.data_access.artist_repository;

import com.example.truthaboutdatabase.data_access.utils.DatabaseConnectionFactory;
import com.example.truthaboutdatabase.models.Artist;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ArtistRepositoryImplementation implements ArtistRepository {
    private final DatabaseConnectionFactory connectionFactory;
    // gets database dependency through dependency injection
    public ArtistRepositoryImplementation(DatabaseConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Artist> getRandomArtists() {
        List<Artist> artists = new ArrayList<>();

        try(Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Artist ORDER BY random() LIMIT 5");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                artists.add(new Artist(
                            resultSet.getInt("ArtistId"),
                            resultSet.getString("Name")
                        )
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return artists;
    }
}
