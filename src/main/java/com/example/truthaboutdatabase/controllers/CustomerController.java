package com.example.truthaboutdatabase.controllers;


import com.example.truthaboutdatabase.data_access.customer_repository.CustomerRepository;
import com.example.truthaboutdatabase.models.Customer;
import com.example.truthaboutdatabase.models.CustomerCountry;
import com.example.truthaboutdatabase.models.CustomerGenre;
import com.example.truthaboutdatabase.models.CustomerSpender;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
@Tag(name = "Customer")
public class CustomerController {

    private final CustomerRepository customerRepository;
    // gets customer dependency through dependency injection
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("customers")
    public List<Customer> getAllCustomers() {

        return customerRepository.getAllCustomers();
    }

    @GetMapping("customers/id/{id}")
    public Customer getCustomerById(@PathVariable Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping( "customers/name/{firstName}")
    public List<Customer> getCustomerByName(@PathVariable String firstName) {
        return customerRepository.getCustomerByName(firstName, "");
    }

    @GetMapping("customers/name/{firstName}/{lastName}")
    public List<Customer> getCustomerByName(@PathVariable String firstName, @PathVariable String lastName) {
        return customerRepository.getCustomerByName(firstName, lastName);
    }

    @GetMapping("customers/limit/{limit}")
    public List<Customer> getLimitedCustomers(@PathVariable Integer limit) {

        return customerRepository.getLimitedCustomers(limit, 0);
    }

    @GetMapping( "customers/limit/{limit}/{offset}")
    public List<Customer> getLimitedCustomers(@PathVariable Integer limit, @PathVariable Integer offset) {
        return customerRepository.getLimitedCustomers(limit, offset);
    }

    @PostMapping("customers/add")
    public Boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @PutMapping("customers/update/{id}")
    public Boolean updateCustomer(@PathVariable String id, @RequestBody Customer customer) {
        return customerRepository.updateCustomer(id, customer);
    }

    @GetMapping("customers/country")
    public List<CustomerCountry> getCustomersInCountries() {
        return customerRepository.getCustomersInCountries();
    }

    @GetMapping("customers/invoice/total")
    public List<CustomerSpender> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

    @GetMapping("customers/id/{id}/popular/genre")
    public CustomerGenre getCustomerMostPopularGenre(@PathVariable Integer id) {
        return customerRepository.getCustomerMostPopularGenre(id);
    }


}
