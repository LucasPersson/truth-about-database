package com.example.truthaboutdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TruthAboutDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(TruthAboutDatabaseApplication.class, args);
    }

}
