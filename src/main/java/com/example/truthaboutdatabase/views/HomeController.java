package com.example.truthaboutdatabase.views;

import com.example.truthaboutdatabase.data_access.artist_repository.ArtistRepository;
import com.example.truthaboutdatabase.data_access.genre_repository.GenreRepository;
import com.example.truthaboutdatabase.data_access.track_repository.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.websocket.server.PathParam;

@Controller
public class HomeController {

    private final ArtistRepository artistRepository;
    private final TrackRepository trackRepository;
    private final GenreRepository genreRepository;

    public HomeController(ArtistRepository artistRepository, TrackRepository trackRepository, GenreRepository genreRepository) {
        this.artistRepository = artistRepository;
        this.trackRepository = trackRepository;
        this.genreRepository = genreRepository;
    }

    @GetMapping("")
    public String home(Model model) {
        model.addAttribute("artists", artistRepository.getRandomArtists());
        model.addAttribute("tracks", trackRepository.getRandomTracks());
        model.addAttribute("genres", genreRepository.getRandomGenres());
        model.addAttribute("search", "hej");
        return "home";
    }

    @GetMapping("search")
    public String search(Model model, @PathParam("param") String param) {
        if (param.equals("")) {
            return home(model);
        }
        model.addAttribute("searchResults", trackRepository.getSearchResults(param));
        model.addAttribute("searchString", param);
        return "home";
    }

}
