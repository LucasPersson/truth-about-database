# Truth About Database

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=heroku&message=Online&label=Heroku&color=430098)](https://salty-refuge-81318.herokuapp.com)
[![container](https://img.shields.io/static/v1?logo=docker&message=Registry&label=Docker&color=2496ED)](https://gitlab.com/LucasPersson/truth-about-database/container_registry)
[![pipeline status](https://gitlab.com/noroff-accelerate/java/projects/spring-with-ci/badges/master/pipeline.svg)](https://gitlab.com/LucasPersson/truth-about-database/-/pipelines)

A Spring Boot application in Java. Access and Expose a Database.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Testing](#testing)
- [Assumptions](#assumptions)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project is created for the purpose of learning backend API and endpoints with a database.
The assignment was to be completed through pair programing. 

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

For Linux/macOS users, open a terminal and run:

```sh
./gradlew bootRun
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

## Testing

For testing the endpoint of the application you can use the Swagger documentation UI at /swagger-ui/index.html.

## Assumptions

In the assignment we have made the following assumptions.
- That the database contains no duplicate artists connected to a track.
- In the specification 3.8 we assume that we should display all customers who have made a purchase.

## Maintainers

[Lucas Persson (@LucasPersson)](https://gitlab.com/LucasPersson), [Edwin Eliasson (@edwineliasson98)](https://gitlab.com/edwineliasson98)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Lucas Persson, Edwin Eliasson
